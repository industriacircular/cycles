import logging
import requests
from django.conf import settings


def explode_on_get(*args, **kwargs):
    raise RuntimeError(
        "The test tried to make a GET request to the network. You are probably missing a mock somewhere"
    )


requests.get = explode_on_get
settings.GOOGLE_API_KEY = "000000000000000000000000000000000000000"
logging.disable(logging.CRITICAL)
