import pytest
from cycles.core.models import Company
from cycles.selfservice.views import ResidueProducedForm


class TestResidueProducedForm:
    @pytest.fixture
    def company(self, db):
        return Company.objects.create(name="The company")

    @pytest.mark.parametrize("field", ("material", "destination", "packaging"))
    def test_add_new_related_model(self, company, field):
        form = ResidueProducedForm(data={field: "add", f"{field}_name": f"New {field}"})
        assert field not in form.errors
        assert form.cleaned_data[field].name == f"New {field}"

    @pytest.mark.parametrize("field", ("material", "destination", "packaging"))
    def test_add_new_related_model_when_nested(self, company, field):
        form = ResidueProducedForm(
            data={f"foo-{field}": "add", f"foo-{field}_name": f"New {field}"},
            prefix="foo",
        )
        assert field not in form.errors
        assert form.cleaned_data[field].name == f"New {field}"
