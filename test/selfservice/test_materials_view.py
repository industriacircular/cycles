import pytest
from django.test import Client
from cycles.core.models import Company


class TestMaterialsView:
    @pytest.fixture(autouse=True)
    def setup(self, db):
        self.client = Client()

    def test_basic(self):
        company = Company.objects.create(name="foo")
        r = self.client.get(f"/selfservice/{company.secret_code}/materials/")
        assert r.status_code == 200
