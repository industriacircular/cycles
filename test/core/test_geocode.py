from django.test import SimpleTestCase
from unittest.mock import patch
import json


from cycles.core.geocode import find_place
from cycles.core.geocode import LocationNotFound
from cycles.core.geocode import MultipleLocations
from cycles.core.geocode import get_phone
from cycles.core.geocode import PhoneNotFound
from cycles.core.geocode import geocode


SAMPLE_FIND_PLACE_RESPONSE = {
    "status": "OK",
    "candidates": [
        {
            "formatted_address": "Rua Desembargador Motta, 1070 - Água Verde, Curitiba - PR, 80250-060, Brazil",
            "geometry": {
                "location": {"lat": -25.4442475, "lng": -49.27628800000001},
                "viewport": {
                    "northeast": {"lat": -25.44293232010728, "lng": -49.27507362010729},
                    "southwest": {"lat": -25.44563197989272, "lng": -49.27777327989273},
                },
            },
            "name": "Hospital Pequeno Príncipe",
            "placeid": "ChIJmXfP9Xrk3JQR2LImorcj54U",
        },
    ],
}

SAMPLE_FIND_PLACES_MULTIPLE_LOCATIONS = {
    "status": "OK",
    "candidates": [
        {
            "name": "First Place",
        },
        {
            "name": "Second Place",
        },
    ],
}

SAMPLE_GET_PHONE_RESPONSE = {
    "status": "OK",
    "result": {"formatted_phone_number": "+55 41 9999-9999"},
}


class FindPlaceTest(SimpleTestCase):
    @patch("requests.get")
    def test_find_place(self, get):
        get.return_value.content = json.dumps(SAMPLE_FIND_PLACE_RESPONSE)
        data = find_place("foo bar")
        self.assertEqual("Hospital Pequeno Príncipe", data["name"])

    @patch("requests.get")
    def test_find_place_multiple_responses(self, get):
        get.return_value.content = json.dumps(SAMPLE_FIND_PLACES_MULTIPLE_LOCATIONS)
        with self.assertRaises(MultipleLocations):
            find_place("foo bar")

    @patch("requests.get")
    def test_find_place_not_found(self, get):
        get.return_value.content = '{"status": "FAIL"}'
        with self.assertRaises(LocationNotFound):
            find_place("foo bar")


class GetPhoneTest(SimpleTestCase):
    @patch("requests.get")
    def test_get_phone_not_found(self, get):
        get.return_value.content = '{"status": "FAIL"}'
        with self.assertRaises(PhoneNotFound):
            get_phone("1239123927139712")

    @patch("requests.get")
    def test_get_phone_found(self, get):
        get.return_value.content = json.dumps(SAMPLE_GET_PHONE_RESPONSE)
        self.assertEqual(get_phone("1239123927139712"), "+55 41 9999-9999")


class GeoCodeTest(SimpleTestCase):
    @patch("requests.get")
    def test_geocode(self, get):
        get.return_value.content = json.dumps(SAMPLE_FIND_PLACE_RESPONSE)
        point = geocode("foo bar")
        self.assertAlmostEqual(-49.27628800000001, point.x)
        self.assertAlmostEqual(-25.4442475, point.y)

    @patch("requests.get")
    def test_geocode_failure(self, get):
        get.return_value.content = '{"status": "FAIL"}'
        point = geocode("foo bar")
        self.assertIsNone(point)
