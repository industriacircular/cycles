from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.test import Client
import json
import pytest
from unittest.mock import MagicMock
from cycles.core.admin import CompanyAdmin
from cycles.core.admin import geocode
from cycles.core.admin import FieldOfActivityFilter
from cycles.core.admin import HasGeoLocationFilter
from cycles.core.admin import ProducersList
from cycles.core.models import Company
from cycles.core.models import FieldOfActivity


class TestCompanyAdmin:
    def test_field_of_activity_display_name(self):
        company = MagicMock()
        company.field_of_activity = MagicMock()
        company.field_of_activity.name = "x" * 100

        admin = CompanyAdmin(MagicMock(), MagicMock())
        assert len(admin.field_of_activity_display(company)) == 60

    def test_field_of_activity_display_name_none(self):
        company = MagicMock()
        company.field_of_activity = None

        admin = CompanyAdmin(MagicMock(), MagicMock())
        assert admin.field_of_activity_display(company) is None

    def test_get_context(self):
        company = MagicMock()
        company.field_of_activity = None
        admin = CompanyAdmin(MagicMock(), MagicMock())

        assert admin.get_context(company) == {"company": company}


class TestCompanyAdminMap:
    @pytest.fixture(autouse=True)
    def setup(self, db):
        self.client = Client()
        self.client.force_login(
            User.objects.create(username="admin", is_superuser=True, is_staff=True)
        )

    def test_map_link(self, db):
        response = self.client.get("/admin/core/company/?q=foobar")
        assert 'href="map/?q=foobar"' in str(response.content)

    def test_show_map(self):
        response = self.client.get("/admin/core/company/map/?q=foobar")
        assert 'href="../?q=foobar"' in str(response.content)

    def test_mapdata(self, db):
        Company.objects.create(name="foo", location=Point(1, 1))
        Company.objects.create(name="bar", location=Point(3, 3))
        response = self.client.get(
            "/admin/core/company/mapdata/",
            {
                "north": "0",
                "west": "0",
                "south": "2",
                "east": "2",
            },
        )
        data = json.loads(response.content)
        assert len(data["features"]) == 1

    def test_mapdata_with_search(self, db):
        Company.objects.create(name="foo", location=Point(1, 1))
        Company.objects.create(name="bar", location=Point(1, 1))
        Company.objects.create(name="baz", location=Point(3, 3))
        response = self.client.get(
            "/admin/core/company/mapdata/",
            {
                "q": "foo",
                "north": "0",
                "west": "0",
                "south": "2",
                "east": "2",
            },
        )
        data = json.loads(response.content)
        assert len(data["features"]) == 1


class TestFieldOfActivityFilter:
    def test_lookups(self, db):
        yes = FieldOfActivity.objects.create(code="1", name="YES", use_as_filter=True)
        no = FieldOfActivity.objects.create(code="2", name="NO", use_as_filter=False)
        __filter__ = self.create_filter(MagicMock())

        lookups = __filter__.lookups(MagicMock(), MagicMock())
        assert (yes.code, str(yes)) in lookups
        assert (no.code, str(no)) not in lookups

    @pytest.fixture
    def create_companies(self, db):
        field = FieldOfActivity.objects.create(code="1", name="YES", use_as_filter=True)
        self.yes = Company.objects.create(field_of_activity=field)
        self.no = Company.objects.create()

    def create_filter(self, params):
        return FieldOfActivityFilter(MagicMock(), params, MagicMock(), MagicMock())

    def test_query(self, create_companies):
        __filter__ = self.create_filter({"field_of_activity": "1"})
        queryset = __filter__.queryset(MagicMock(), Company.objects.all())
        assert self.yes in queryset
        assert self.no not in queryset

    def test_query_none(self, create_companies):
        params = {"field_of_activity": "_"}
        __filter__ = FieldOfActivityFilter(
            MagicMock(), params, MagicMock(), MagicMock()
        )
        queryset = __filter__.queryset(MagicMock(), Company.objects.all())
        assert self.no in queryset
        assert self.yes not in queryset


class TestHasGeoLocationFilter:
    def test_lookups(self):
        __filter__ = HasGeoLocationFilter(
            MagicMock(), MagicMock(), MagicMock(), MagicMock()
        )
        lookups = __filter__.lookups(MagicMock(), MagicMock())
        assert lookups[0][0] == "0"
        assert lookups[1][0] == "1"

    @pytest.fixture
    def create_companies(self, db):
        self.yes = Company.objects.create(name="foo", location=Point(1, 1))
        self.no = Company.objects.create(name="bar", location=None)

    def test_queryset_no(self, db, create_companies):
        params = {"has_geolocation": "0"}
        __filter__ = HasGeoLocationFilter(MagicMock(), params, MagicMock(), MagicMock())
        queryset = __filter__.queryset(MagicMock(), Company.objects.all())
        assert self.no in queryset
        assert self.yes not in queryset

    def test_queryset_yes(self, db, create_companies):
        params = {"has_geolocation": "1"}
        __filter__ = HasGeoLocationFilter(MagicMock(), params, MagicMock(), MagicMock())
        queryset = __filter__.queryset(MagicMock(), Company.objects.all())
        assert self.yes in queryset
        assert self.no not in queryset

    def test_queryset_all(self, db, create_companies):
        params = {}
        __filter__ = HasGeoLocationFilter(MagicMock(), params, MagicMock(), MagicMock())
        queryset = __filter__.queryset(MagicMock(), Company.objects.all())
        assert self.yes in queryset
        assert self.no in queryset


class TestGeoCode:
    def test_basics(self, db, mocker):
        company_geocode = mocker.patch("cycles.core.models.CompanyBase.geocode")
        Company.objects.create()
        geocode(None, None, Company.objects.all())
        company_geocode.assert_called_once()


class TestCompanyList:
    def test_permissions(self):
        admin = ProducersList(MagicMock(), MagicMock())
        assert not admin.has_add_permission(MagicMock())
        assert not admin.has_delete_permission(MagicMock())
