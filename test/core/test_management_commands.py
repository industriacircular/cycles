import json
from django.test import TestCase
from django.core.management import call_command
from unittest.mock import patch, ANY, mock_open

from cycles.core.models import Company
from cycles.core.models import FieldOfActivity


class SearchCompanyTest(TestCase):
    @patch("cycles.core.geocode.find_place")
    def test_basic_search(self, find_place):
        find_place.return_value = {
            "name": "Foo Bar",
            "geometry": {
                "location": {
                    "lng": 12,
                    "lat": 34,
                }
            },
        }
        call_command("search_company", "foo bar")
        company = Company.objects.get(name="Foo Bar")
        self.assertEqual((company.location.x, company.location.y), (12, 34))

    @patch("cycles.core.geocode.find_place")
    def test_dry_run(self, find_place):
        call_command("search_company", "--dry-run", "foo bar")
        find_place.assert_not_called()


SAMPLE_CNAE = [
    {
        "id": "1610204",
        "descricao": "SERRARIAS SEM DESDOBRAMENTO DE MADEIRA EM BRUTO - RESSERRAGEM",
        "classe": {
            "id": "16102",
            "descricao": "DESDOBRAMENTO DE MADEIRA",
            "grupo": {
                "id": "161",
                "descricao": "DESDOBRAMENTO DE MADEIRA",
                "divisao": {
                    "id": "16",
                    "descricao": "FABRICAÇÃO DE PRODUTOS DE MADEIRA",
                    "secao": {"id": "C", "descricao": "INDÚSTRIAS DE TRANSFORMAÇÃO"},
                },
            },
            "observacoes": [
                "Esta classe compreende - a fabricação de madeira bruta desdobrada (serrada) e de madeira resserrada (pranchas, pranchões, postes, tábuas, barrotes, aplainados para caixas e engradados e semelhantes)\r\n- a fabricação de pisos de madeira e tábuas para assoalho e teto\r\n- a fabricação de dormentes para vias férreas",
                "Esta classe compreende ainda - a fabricação de lã e de partículas de madeira para qualquer fim\r\n- a secagem, preservação e imunização da madeira\r\n- a fabricação de briquetes de resíduos de madeira (carvão ecológico)",
                "Esta classe NÃO compreende - a produção florestal de madeira em bruto (divisão 02)\r\n- a fabricação de chapas de madeira compensada, prensada ou aglomerada (16.21-8)\r\n- a fabricação de estruturas de madeira e vigamentos para construção (16.22-6)",
            ],
        },
        "atividades": ["FORROS DE MADEIRA; PRODUÇÃO DE"],
        "observacoes": [
            "Esta subclasse compreende - a produção de madeira resserrada submetida a aplainamento, secagem ou lixamento (pranchas, pranchões, postes, tábuas, tacos e parquetes para assoalhos e semelhantes)\r\n- a fabricação de forros de madeira\r\n- a fabricação de dormentes para vias férreas",
            "Esta subclasse compreende ainda - a fabricação de lã e de partículas de madeira para qualquer fim\r\n- a secagem, preservação e imunização da madeira quando integrada à produção de madeira resserrada",
            "Esta subclasse NÃO compreende - a extração florestal de madeira em bruto (seção A - divisão 02)\r\n- as serrarias com desdobramento de madeira em bruto (1610-2/03)\r\n- os serviços de tratamento de madeira realizada sob contrato (1610-2/05)\r\n- a fabricação de chapas de madeira compensada, prensada ou aglomerada (1621-8/00)\r\n- a fabricação de estruturas de madeira e vigamentos para construção (1622-6/99)",
        ],
    }
]


class ImportCNAETest(TestCase):
    @patch("requests.get")
    def test_basics(self, get):
        get.return_value.content = json.dumps(SAMPLE_CNAE)
        call_command("import_cnae", "C")
        get.assert_called_with(ANY)

        self.assertEqual(1, FieldOfActivity.objects.filter(code="1610204").count())
        self.assertEqual(1, FieldOfActivity.objects.filter(code="16102").count())
        self.assertEqual(1, FieldOfActivity.objects.filter(code="161").count())
        self.assertEqual(1, FieldOfActivity.objects.filter(code="161").count())
        self.assertEqual(1, FieldOfActivity.objects.filter(code="16").count())


SAMPLE_DATA_FROM_CADASTROINDUSTRIALPR = {
    "status": "sucess",
    "total": 1,
    "records": [
        {
            "recid": "9999",
            "cnpj": "01234567890123",
            "nome": "INDÚSTRIA XXXXXXXXXX XX XXXXXXXXXX LTDA",
            "fantasia": "XXX AXXXXXXXX",
            "endereco": "Rua XXXXXXXX XXXXXX, 999",
            "tipo_logradouro": "",
            "logradouro": "",
            "numero": "",
            "complemento": "",
            "bairro": "XXXXX",
            "cep": "80000000",
            "municipio": "Curitiba",
            "uf": "PR",
            "ddd_telefone": "41",
            "telefone": "99999999",
            "pagina_web": "http://www.xxxxxxxxxxxx.com.br",
            "email": "xxxxxx@xxxx.com.br",
            "email_compras": "xxxxxx@xxxx.com.br",
            "email_vendas": "",
            "nro_funcionarios": "1",
            "cnae": "7777777",
            "descricao": "",
            "exporta": "N",
            "importa": "N",
            "produto_1": "",
            "produto_2": "",
            "produto_3": "",
        },
    ],
}


class ImportCadastroIndustrialPRTest(TestCase):
    @patch("cycles.core.models.Company.maybe_geocode")
    def test_basics(self, maybe_geocode):
        data = json.dumps(SAMPLE_DATA_FROM_CADASTROINDUSTRIALPR)
        with patch(
            "cycles.core.management.commands.import_cadastroindustrialpr.open",
            mock_open(read_data=data),
        ) as m:
            call_command("import_cadastroindustrialpr", "foo.json")
        m.assert_called_with("foo.json")

        c = Company.objects.get(cnpj="01234567890123")
        f = FieldOfActivity.objects.get(code="7777777")
        self.assertEqual(c.field_of_activity, f)
