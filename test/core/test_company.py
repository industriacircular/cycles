from django.core.exceptions import ValidationError
from django.contrib.gis.geos import Point
from django.test import TestCase
from unittest.mock import patch


from cycles.core import models


class ModelValidationTestCase:
    def __get_validation_errors__(self, model):
        try:
            model.full_clean()
            return []
        except ValidationError as e:
            return e.message_dict

    def assertValidField(self, model, field):
        self.assertNotIn(field, self.__get_validation_errors__(model))

    def assertInvalidField(self, model, field):
        self.assertIn(field, self.__get_validation_errors__(model))


class CompanyTest(TestCase, ModelValidationTestCase):
    def test_str(self):
        company = models.Company(name="foo")
        self.assertEqual("foo", str(company))

    def test_order_by_name(self):
        models.Company.objects.create(name="bbb")
        aaa = models.Company.objects.create(name="aaa")
        self.assertEqual(aaa, models.Company.objects.first())

    def test_invalid_cnpj(self):
        company = models.Company(name="Foo", cnpj="00.000.000/0000-00")
        self.assertInvalidField(company, "cnpj")

    def test_valid_field(self):
        company = models.Company(name="Foo", cnpj="33.202.794/0001-25")
        self.assertValidField(company, "cnpj")

    def test_unique_cnpj(self):
        models.Company.objects.create(name="Foo", cnpj="33.202.794/0001-25")
        company = models.Company(name="Bar", cnpj="33.202.794/0001-25")
        self.assertInvalidField(company, "cnpj")

    def test_empty_cnpj(self):
        company = models.Company(name="Foo")
        self.assertValidField(company, "cnpj")
        company.cnpj = ""
        self.assertValidField(company, "cnpj")

    @patch("cycles.core.models.Company.geocode")
    def test_geocode_on_create(self, geocode):
        models.Company.objects.create(name="Foo", address="bla bla bla")
        geocode.assert_called()

    @patch("cycles.core.models.Company.geocode")
    def test_geocode_on_update(self, geocode):
        company = models.Company.objects.create(name="Foo", address="bla bla bla")
        geocode.reset_mock()
        company.address = "foo bar road, 11"
        company.save()
        geocode.assert_called()

    @patch("cycles.core.models.Company.geocode")
    def test_geocode_on_update_only_when_address_changes(self, geocode):
        company = models.Company.objects.create(name="Foo", address="bla bla bla")
        geocode.reset_mock()
        company.name = "Foo Bar"
        company.save()
        geocode.assert_not_called()

    @patch("cycles.core.models.Company.geocode")
    def test_wont_geocode_when_location_is_alredy_provided(self, geocode):
        models.Company.objects.create(
            name="Foo", address="foo bar", location=Point(1, 2)
        )
        geocode.assert_not_called()

    @patch("cycles.core.geocode.geocode")
    def test_geocode(self, geocode):
        c = models.Company(address="foo bar")
        geocode.return_value = Point(1, 2)
        c.geocode()
        self.assertEqual((1, 2), (c.location.x, c.location.y))

    @patch("cycles.core.models.Company.geocode")
    @patch("cycles.core.models.settings")
    def test_skip_geocode_on_create(self, settings, geocode):
        settings.SKIP_GEOCODING = True
        models.Company.objects.create(name="Foo", address="bla bla bla")
        geocode.assert_not_called()

    def test_in_region(self):
        c1 = models.Company.objects.create(name="foo", location=Point(1, 1))
        c2 = models.Company.objects.create(name="bar", location=Point(3, 3))
        region = models.get_polygon(
            south=0,
            north=2,
            west=0,
            east=2,
        )
        subset = models.Company.objects.filter(location__contained=region)

        self.assertTrue(c1 in subset)
        self.assertFalse(c2 in subset)

    def test_secret_id(self):
        c1 = models.Company.objects.create(name="Foo")
        assert c1.secret_code is not None
