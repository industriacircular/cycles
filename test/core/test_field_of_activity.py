from cycles.core.models import FieldOfActivity


def test_str():
    field = FieldOfActivity(name="Foo", code="123")
    assert str(field) == "123 - Foo"


def test_str_without_code():
    field = FieldOfActivity(name="Foo")
    assert str(field) == "Foo"
