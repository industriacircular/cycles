#!/bin/sh

set -eu

if [ $# -lt 2 ]; then
  echo "usage: $0 COOKIE ATIVIDADE [ATIVIDADE ...]"
  exit 1
fi

cookie="$1"
shift

for atividade in $@; do
  curl \
    --cookie PHPSESSID=$cookie \
    --output "${atividade}.json" \
    --data "search[0][field]=atividade&search[0][value]=${atividade}&search[0][operator]=begins&search[1][field]=internacional&search[1][value]=Todos&search[1][operator]=is&search[2][field]=funcionario&search[2][value]=0&search[2][operator]=equals" \
    http://cadastroindustrialpr.com.br/services/navegador/lista_empresasJSON.php
done
