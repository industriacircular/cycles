#!/bin/sh

set -exu

apt-get update -q=2

apt-get install -q=2 --no-install-recommends \
    fonts-fork-awesome \
    gettext \
    gunicorn \
    spatialite-bin \
    libsqlite3-mod-spatialite \
    libjs-jquery \
    libjs-bootstrap4 \
    python3-daiquiri \
    python3-django \
    python3-django-compressor \
    python3-django-crispy-forms \
    python3-django-libsass \
    python3-django-extra-views \
    python3-django-mptt \
    python3-gdal \
    python3-requests \
    python3-stdnum \
    python3-whitenoise
