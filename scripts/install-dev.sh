#!/bin/sh

set -exu

apt-get update -q=2

apt-get install -q=2 \
    black \
    flake8 \
    gettext \
    python3-django-debug-toolbar \
    python3-django-extensions \
    python3-pytest \
    python3-pytest-cov \
    python3-pytest-django \
    python3-pytest-mock
