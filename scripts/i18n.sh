#!/bin/sh

set -eu

LANGUAGES=$(ls -1d cycles/*/locale/*/LC_MESSAGES | cut -d / -f 4 | sort -u)
MODULES="${1:-core selfservice}"

for app in $MODULES; do

  for language in $LANGUAGES; do
    mkdir -p cycles/${app}/locale/${language}/LC_MESSAGES
  done

  # extract messages
  pot=cycles/$app/locale/django.pot
  pybabel extract \
    --mapping-file babel.cfg \
    --project cycles.$app \
    --no-location \
    --output $pot \
    cycles/$app

  if [ "$(msgattrib "${pot}" | wc -l)" -eq 0 ]; then
    rm -f "${pot}"
    continue
  fi

  # merge with existing translations
  for language in $LANGUAGES; do
    po=cycles/${app}/locale/${language}/LC_MESSAGES/django.po
    if [ -f "$po" ]; then
      cp $po ${po}.old
      msgmerge --output "${po}" "${po}.old" cycles/$app/locale/django.pot
      rm -f "${po}.old"
    else
      cp ${pot} ${po}
      echo "Creating ${po}"
    fi
  done

  rm -f "$pot"

done

./manage.py compilemessages
find cycles/*/locale -empty -delete
