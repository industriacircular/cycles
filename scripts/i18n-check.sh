#!/bin/sh

set -eu

rc=0

export LANG='C.UTF.8'
export LC_ALL='C.UTF-8'
export LANGUAGE=

for po in cycles/*/locale/*/LC_MESSAGES/django.po; do
  output="$(msgfmt --output=/dev/null --statistics "${po}" 2>&1)"
  echo "${po}: ${output}"
  case "${output}" in
    *fuzzy*|*untranslated*)
      rc=1
      ;;
  esac
done
if [ "${rc}" -ne 0 ]; then
  echo "FAIL" >&2
fi

exit "${rc}"
