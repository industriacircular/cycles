FROM debian:bullseye-slim
COPY scripts/install.sh /install.sh
RUN /install.sh

WORKDIR /app
COPY . ./

ENV DEBUG=
RUN cycles/core/static/download && \
    ./manage.py collectstatic --noinput --verbosity 0 \
        --ignore=download \
        --ignore=download.conf \
        --ignore=download.status \
        --ignore='vendor/*.tar' && \
    ./manage.py compress --force && \
    ./manage.py compilemessages

USER nobody
ENV DATABASE=/data/db.sqlite3
ENV SECRET_KEY_FILE=/data/secret.dat
CMD sh -c './manage.py migrate && gunicorn3 cycles.wsgi 0.0.0.0:$PORT'
