all: tests style i18n

tests:
	python3 -m pytest --cov --cov-fail-under=94

style:
	flake8
	black --check .

i18n:
	./scripts/i18n-check.sh
