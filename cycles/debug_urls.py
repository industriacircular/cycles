import debug_toolbar
from django.urls import include, path
import cycles.urls

urlpatterns = cycles.urls.urlpatterns + [
    path("__debug__/", include(debug_toolbar.urls)),
]
