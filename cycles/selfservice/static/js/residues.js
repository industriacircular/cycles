jQuery(function($) {

  // add/select-existing fields
  $('button.select-existing').hide()
  $('div.add-new').hide()
  $("button.add-new").click(function() {
    $(this).closest(".select-or-add").find('.add-new , .select-existing').toggle()
    $(this).closest(".select-or-add").find('.select-existing select').focus()
  })
  $('button.select-existing').click(function() {
    $(this).closest(".select-or-add").find('.add-new , .select-existing').toggle()
    $(this).closest(".select-or-add").find(".add-new input").val("")
    $(this).closest(".select-or-add").find(".select-existing input").focus()
  })


  // destination cost/income
  $("#id_destination_cost_or_income").change(function() {
    var hide;
    var show;
    if ($(this).val() == "cost") {
      $("#id_destination_income").closest(".form-group").parent().hide()
      $("#id_destination_income").closest(".form-group").parent().find('input, select').val("")
      $("#id_destination_cost").closest(".form-group").parent().show()
    } else {
      $("#id_destination_income").closest(".form-group").parent().show()
      $("#id_destination_cost").closest(".form-group").parent().hide()
      $("#id_destination_cost").closest(".form-group").parent().find("input, select").val("")
    }
  })
  $("#id_destination_cost_or_income").change()


})
