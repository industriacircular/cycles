from django.urls import path
from .views import MaterialsView
from .views import UpdateResidueView
from .views import CreateResidueView
from .views import DeleteResidueView

urlpatterns = [
    path(
        "<secret_code>/materials/",
        MaterialsView.as_view(),
        name="selfservice-materials",
    ),
    path(
        "<secret_code>/materials/residue/new/",
        CreateResidueView.as_view(),
        name="selfservice-residue-create",
    ),
    path(
        "<secret_code>/materials/residue/<pk>/",
        UpdateResidueView.as_view(),
        name="selfservice-residue-update",
    ),
    path(
        "<secret_code>/materials/residue/<pk>/delete/",
        DeleteResidueView.as_view(),
        name="selfservice-residue-delete",
    ),
]
