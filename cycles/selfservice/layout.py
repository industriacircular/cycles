from django.utils.translation import gettext_lazy as _
from crispy_forms.layout import Div, Submit, HTML, Field
from crispy_forms.bootstrap import StrictButton, FormActions


class Row(Div):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, css_class="row", **kwargs)


class Col(Div):
    def __init__(self, *args, width=None, css_class=None, **kwargs):
        if width:
            cls = f"col-md-{width}"
        else:
            cls = "col"
        kwargs = kwargs.copy()
        super().__init__(*args, css_class=cls, **kwargs)


class SelectOrAdd(Div):
    def __init__(self, field):
        super().__init__(
            SelectExisting(Field(field)),
            AddNew(Field(f"{field}_name")),
            AddNewButton(),
            SelectExistingButton(),
            css_class="select-or-add",
        )


class AddNew(Div):
    def __init__(self, field):
        super().__init__(field, css_class="add-new")


class AddNewButton(StrictButton):
    def __init__(self):
        super().__init__(_("Add new"), css_class="btn-light btn-sm add-new")


class SelectExisting(Div):
    def __init__(self, field):
        super().__init__(field, css_class="select-existing")


class SelectExistingButton(StrictButton):
    def __init__(self):
        super().__init__(
            _("Select existing"), css_class="btn-light btn-sm select-existing"
        )


class FormButtons(FormActions):
    def __init__(self, cancel_url=".."):
        super().__init__(
            Submit("save", _("Save")),
            HTML(
                '<a class="btn btn-danger" href="%(link)s">%(label)s</a>'
                % {"label": _("Cancel"), "link": cancel_url}
            ),
            css_class="form-buttons",
        )
