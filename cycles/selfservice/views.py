from django.conf import settings
from django.forms import ModelForm, CharField, ChoiceField
from django.urls import reverse
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.utils.translation import gettext_lazy as _
from mptt.forms import TreeNodeChoiceField
from cycles.core.models import (
    Company,
    ResidueProduced,
    Material,
    DestinationType,
    PackagingType,
)

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Field
from crispy_forms.bootstrap import AppendedText
from crispy_forms.bootstrap import PrependedText
from cycles.selfservice.layout import (
    Row,
    Col,
    SelectOrAdd,
    FormButtons,
)


class MaterialChoiceField(TreeNodeChoiceField):
    def __init__(self, **kwargs):
        return super().__init__(
            **kwargs,
            queryset=Material.objects.all(),
            level_indicator="  ",  # 2x No-break space (U+0A)
        )


class ResidueProducedForm(ModelForm):
    material = MaterialChoiceField(required=False)
    material_name = CharField(required=False, label=_("Material"))
    destination_name = CharField(required=False, label=_("Destination"))
    packaging_name = CharField(required=False, label=_("Packaging"))
    destination_cost_or_income = ChoiceField(
        label=_("Generates cost or income?"),
        choices=(
            ("cost", _("Cost")),
            ("income", _("Income")),
        ),
        required=False,
    )
    auto_add_fields = {
        "material_name": Material,
        "destination_name": DestinationType,
        "packaging_name": PackagingType,
    }

    class Meta:
        model = ResidueProduced
        fields = (
            "material",
            "material_name",
            "quantity",
            "quantity_unit",
            # storage
            "packaging",
            "packaging_name",
            "equipment_rent_cost",
            # destination
            "destination",
            "destination_name",
            "destination_company",
            "destination_cost_or_income",
            "destination_income",
            "destination_income_unit",
            "destination_cost",
            "destination_cost_unit",
            # transport
            "transport_company",
            "transport_cost",
            "transport_cost_unit",
            "frequency",
            "frequency_unit",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

        # labels
        self.fields["destination_cost"].label = _("Cost")
        self.fields["destination_cost_unit"].label = _("Unit")
        self.fields["destination_income"].label = _("Income")
        self.fields["destination_income_unit"].label = _("Unit")
        self.fields["transport_company"].label = _("Company")
        self.fields["transport_cost"].label = _("Cost")
        self.fields["transport_cost_unit"].label = _("Unit")
        self.fields["frequency_unit"].label = _("Unit")

        # values
        if self.instance.destination_cost:
            self.fields["destination_cost_or_income"].initial = "cost"
        elif self.instance.destination_income:
            self.fields["destination_cost_or_income"].initial = "income"

        self.helper.layout = Layout(
            Fieldset(
                _("Residue produced"),
                Row(
                    Col(SelectOrAdd("material"), width=6),
                    Col("quantity", "quantity_unit", width=6),
                ),
            ),
            Fieldset(
                _("Storage"),
                Row(
                    Col(SelectOrAdd("packaging"), width=4),
                    Col(
                        PrependedText("equipment_rent_cost", settings.CURRENCY),
                        width=4,
                    ),
                ),
            ),
            Fieldset(
                _("Destination"),
                Row(
                    Col(SelectOrAdd("destination"), width=4),
                    Col(Field("destination_company"), width=4),
                ),
                Row(
                    Col("destination_cost_or_income", width=4),
                    Col(
                        PrependedText("destination_income", settings.CURRENCY),
                        PrependedText("destination_income_unit", _("per")),
                        width=4,
                    ),
                    Col(
                        PrependedText("destination_cost", settings.CURRENCY),
                        PrependedText("destination_cost_unit", _("per")),
                        width=8,
                    ),
                ),
            ),
            Fieldset(
                _("Transport"),
                Row(
                    Col("transport_company", width=4),
                ),
                Row(
                    Col(
                        PrependedText("transport_cost", settings.CURRENCY),
                        PrependedText("transport_cost_unit", _("per")),
                    ),
                ),
                Row(
                    Col(
                        AppendedText("frequency", _("times")),
                        PrependedText("frequency_unit", _("per")),
                    ),
                ),
            ),
            FormButtons(cancel_url="../.."),
        )

    def full_clean(self):
        self.data = self.data.copy()
        prefix = self.prefix and (self.prefix + "-") or ""
        for field, klass in self.auto_add_fields.items():
            pfield = prefix + field
            if pfield in self.data and self.data[pfield]:
                self.fields[field].choices = klass.objects.all()
                value = self.data[pfield].strip()
                target_field = prefix + field.replace("_name", "")
                self.data[target_field] = klass.objects.create(name=value).pk
                self.data.pop(pfield)
        super().full_clean()

    def clean(self):
        super().clean()
        # material
        if not self.cleaned_data["material"] and not self.cleaned_data["material_name"]:
            self.add_error(
                "material", _("You need to select a material or add a a new one")
            )
        # numbers with units
        for f in self.fields.keys():
            unit = f"{f}_unit"
            if unit in self.fields:
                if self.cleaned_data[f] and not self.cleaned_data[unit]:
                    self.add_error(unit, _("You need to select an unit"))
                elif self.cleaned_data[unit] and not self.cleaned_data[f]:
                    self.add_error(f, _("You need to provide a number"))


class CompanyMixin:
    def get_company(self):
        secret_code = self.kwargs["secret_code"]
        return Company.objects.get(secret_code=secret_code)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["company"] = self.get_company()
        return context


class MaterialsView(CompanyMixin, DetailView):
    model = Company
    template_name = "cycles.selfservice/materials.html"

    def get_object(self):
        return self.get_company()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        company = self.get_company()
        context["residues"] = ResidueProduced.objects.filter(company=company)
        return context


class ResidueMixin:
    model = ResidueProduced
    form_class = ResidueProducedForm

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(
                company__secret_code=self.kwargs["secret_code"],
            )
        )

    def get_success_url(self):
        secret_code = self.get_company().secret_code
        return reverse("selfservice-materials", args=[secret_code])


class UpdateResidueView(ResidueMixin, CompanyMixin, UpdateView):
    template_name = "cycles.selfservice/residue.html"


class CreateResidueView(ResidueMixin, CompanyMixin, CreateView):
    template_name = "cycles.selfservice/residue.html"

    def form_valid(self, form):
        form.instance.company = self.get_company()
        return super().form_valid(form)


class DeleteResidueView(ResidueMixin, CompanyMixin, DeleteView):
    template_name = "cycles.selfservice/residue_confirm_delete.html"
