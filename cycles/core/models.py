import uuid
from django.conf import settings
from django.core.exceptions import ValidationError
from django.contrib.gis.db import models
from django.contrib.gis.geos import Polygon
from django.db.models.functions import Lower
from django.utils.translation import gettext_lazy as N_
from mptt.models import MPTTModel, TreeForeignKey
from stdnum.br import cnpj


from cycles.core import geocode


def validate_cnpj(v):
    try:
        cnpj.validate(v)
    except Exception as e:
        raise ValidationError(str(e))


class OrderedByName(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by(Lower("name"))


class Named:
    def __str__(self):
        return self.name


class FieldOfActivity(models.Model):
    code = models.CharField(
        max_length=7, verbose_name=N_("Code"), null=True, blank=True
    )
    name = models.CharField(max_length=256, verbose_name=N_("Name"))
    use_as_filter = models.BooleanField(default=False, verbose_name=N_("Use as filter"))
    examples = models.TextField(null=True, blank=True, verbose_name=N_("Examples"))

    def __str__(self):
        return " - ".join([x for x in [self.code, self.name] if x])

    class Meta:
        verbose_name_plural = N_("Fields of activity")
        ordering = ("code",)


class CompanyBase(Named, models.Model):
    name = models.CharField(max_length=128, verbose_name=N_("Name"))
    full_name = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        verbose_name=N_("Full name"),
        help_text=N_("E.g. the legal name of the company"),
    )
    cnpj = models.CharField(
        max_length=18, null=True, blank=True, unique=True, validators=[validate_cnpj]
    )

    email = models.EmailField(null=True, blank=True, verbose_name=N_("Email"))
    phone = models.CharField(
        max_length=40, null=True, blank=True, verbose_name=N_("Phone")
    )

    address = models.TextField(blank=True, null=True, verbose_name=N_("Address"))
    location = models.PointField(blank=True, null=True, verbose_name=N_("Location"))

    secret_code = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        unique=True,
        null=False,
        verbose_name=N_("Secret code"),
        help_text=N_(
            "Secret code used in URLs related to the company. Should not be shared with anyone but the company representatives"
        ),
    )

    def save(self, *args, **kwargs):
        self.maybe_geocode()
        super().save(*args, **kwargs)

    def maybe_geocode(self):
        if settings.SKIP_GEOCODING:
            return
        if self.pk:
            old_data = (
                type(self)
                .objects.values(
                    "address",
                    "location",
                )
                .get(pk=self.pk)
            )
            if self.address == old_data["address"]:
                return
        else:
            if not self.address:
                return
            if self.location:
                return
        self.geocode()

    def geocode(self):
        self.location = geocode.geocode(self.address)

    class Meta:
        abstract = True


def get_polygon(south, north, west, east):
    return Polygon(
        (
            (west, north),
            (east, north),
            (east, south),
            (west, south),
            (west, north),
        )
    )


class Company(CompanyBase):

    field_of_activity = models.ForeignKey(
        FieldOfActivity,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name=N_("Field of activity"),
    )
    activity_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=N_("Descrição das atividades"),
        help_text=N_("Give details about what the company produces."),
    )

    raw_materials = models.ManyToManyField(
        "Material",
        through="RawMaterialUsed",
        related_name="consumed_by",
        verbose_name=N_("Raw materials used"),
        help_text=N_("Raw materials used in the productive process"),
    )

    residues_produced = models.ManyToManyField(
        "Material",
        through="ResidueProduced",
        related_name="produced_by",
        verbose_name=N_("Residues produced"),
        help_text=N_("Residues produced by the productive process"),
    )
    validated = models.BooleanField(default=False, verbose_name=N_("Validated"))

    objects = OrderedByName()

    class Meta:
        verbose_name = N_("Company")
        verbose_name_plural = N_("Companies")


class Product(Named, models.Model):
    name = models.CharField(max_length=128, verbose_name=N_("Name"))
    notes = models.TextField(null=True, blank=True, verbose_name=N_("Notes"))

    objects = OrderedByName()

    class Meta:
        verbose_name = N_("Product")
        verbose_name_plural = N_("Products")


class Material(Named, MPTTModel):
    parent = TreeForeignKey(
        "self",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="children",
        verbose_name=N_("Type of Material"),
        db_index=True,
    )
    name = models.CharField(max_length=128, verbose_name=N_("Name"))
    notes = models.TextField(null=True, blank=True, verbose_name=N_("Notes"))
    applications = models.ManyToManyField(
        Product,
        through="Application",
        verbose_name=N_("Applications"),
        help_text=N_("Products that can be made of this material"),
    )

    objects = OrderedByName()

    class Meta:
        verbose_name = N_("Material")
        verbose_name_plural = N_("Materials")


class Application(models.Model):
    material = models.ForeignKey(
        Material,
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
    )
    notes = models.TextField(
        null=True,
        blank=True,
        verbose_name=N_("Notes"),
    )

    class Meta:
        db_table = "core_material_applications"


class PackagingType(Named, models.Model):
    name = models.CharField(max_length=128, verbose_name=N_("Name"))

    class Meta:
        verbose_name = N_("Packaging type")
        verbose_name_plural = N_("Packaging types")


class DestinationType(Named, models.Model):
    name = models.CharField(max_length=128, verbose_name=N_("Name"))

    class Meta:
        verbose_name = N_("Destination type")
        verbose_name_plural = N_("Destination types")


class MaterialUse(models.Model):
    material = models.ForeignKey(
        Material,
        on_delete=models.CASCADE,
    )
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        verbose_name=N_("Company"),
    )
    quantity = models.FloatField(null=True, blank=True, verbose_name=N_("Quantity"))
    quantity_unit = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        choices=(
            ("kg/d", N_("kg/day")),
            ("kg/w", N_("kg/week")),
            ("kg/m", N_("kg/month")),
            ("kg/y", N_("kg/year")),
            ("t/d", N_("ton/day")),
            ("t/w", N_("ton/week")),
            ("t/m", N_("ton/month")),
            ("t/y", N_("ton/year")),
            ("l/d", N_("l/day")),
            ("l/w", N_("l/week")),
            ("l/m", N_("l/month")),
            ("l/y", N_("l/year")),
            ("m3/d", N_("m³/day")),
            ("m3/w", N_("m³/week")),
            ("m3/m", N_("m³/month")),
            ("m3/y", N_("m³/year")),
            ("un/d", N_("units/day")),
            ("un/w", N_("units/week")),
            ("un/m", N_("units/month")),
            ("un/y", N_("units/year")),
        ),
        verbose_name=N_("Unit"),
    )
    price = models.FloatField(null=True, blank=True, verbose_name=N_("Unit price"))

    class Meta:
        abstract = True


class RawMaterialUsed(MaterialUse):
    pass


PERIODS = (
    ("d", N_("Day")),
    ("w", N_("Week")),
    ("m", N_("Month")),
    ("s", N_("Semester")),
    ("y", N_("Year")),
)


class ResidueProduced(MaterialUse):
    packaging = models.ForeignKey(
        PackagingType,
        null=True,
        blank=True,
        verbose_name=N_("Packaging"),
        on_delete=models.CASCADE,
    )
    destination = models.ForeignKey(
        DestinationType,
        null=True,
        blank=True,
        verbose_name=N_("Destination"),
        on_delete=models.CASCADE,
    )
    destination_company = models.CharField(
        max_length=128, null=True, blank=True, verbose_name=N_("Destination company")
    )
    destination_income = models.FloatField(
        null=True, blank=True, verbose_name=N_("Destination income")
    )
    destination_income_unit = models.CharField(
        null=True,
        blank=True,
        verbose_name=N_("Destination income unit"),
        max_length=2,
        choices=(
            ("kg", N_("kg")),
            ("t", N_("ton")),
            ("l", N_("l")),
            ("m3", N_("m³")),
            ("u", N_("unit")),
        ),
    )

    destination_cost = models.FloatField(
        null=True, blank=True, verbose_name=N_("Destination cost")
    )
    destination_cost_unit = models.CharField(
        null=True,
        blank=True,
        verbose_name=N_("Destination cost unit"),
        max_length=2,
        choices=(
            ("kg", N_("kg")),
            ("t", N_("ton")),
            ("l", N_("l")),
            ("m3", N_("m³")),
            ("u", N_("unit")),
        ),
    )
    transport_company = models.CharField(
        max_length=128, null=True, blank=True, verbose_name=N_("Transport company")
    )
    transport_cost = models.FloatField(
        null=True, blank=True, verbose_name=N_("Transport cost")
    )
    transport_cost_unit = models.CharField(
        null=True,
        blank=True,
        verbose_name=N_("Transport cost unit"),
        max_length=1,
        choices=(
            ("c", N_("Collection")),
            ("l", N_("Load")),
            ("u", N_("Unit")),
            *PERIODS,
        ),
    )
    equipment_rent_cost = models.FloatField(
        null=True, blank=True, verbose_name=N_("Monthly cost with equipment rent")
    )
    frequency = models.IntegerField(null=True, blank=True, verbose_name=N_("Frequency"))
    frequency_unit = models.CharField(
        max_length=1,
        null=True,
        blank=True,
        verbose_name=N_("Frequency unit"),
        choices=PERIODS,
    )


class TransportCompany(CompanyBase):
    class Meta:
        verbose_name = N_("Transport company")
        verbose_name_plural = N_("Transport companies")
