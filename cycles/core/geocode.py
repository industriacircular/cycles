from django.conf import settings
from django.contrib.gis.geos import Point
import json
import logging
import requests


logger = logging.getLogger()


def check_google_api_key(f):
    def wrapped(*args, **kwargs):
        assert settings.GOOGLE_API_KEY
        return f(*args, **kwargs)

    return wrapped


class GeoCodeException(Exception):
    pass


class LocationNotFound(GeoCodeException):
    pass


class MultipleLocations(GeoCodeException):
    pass


@check_google_api_key
def find_place(text):
    url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json"
    params = {
        "key": settings.GOOGLE_API_KEY,
        "input": text,
        "inputtype": "textquery",
        "fields": "place_id,formatted_address,name,geometry",
    }
    response = requests.get(url, params=params)
    data = json.loads(response.content)
    if data["status"] != "OK":
        raise LocationNotFound("Location not found: %s" % text)
    if len(data["candidates"]) > 1:
        raise MultipleLocations("Multiple locations found for %s" % text)
    return data["candidates"][0]


class PhoneNotFound(GeoCodeException):
    pass


@check_google_api_key
def get_phone(placeid):
    url = "https://maps.googleapis.com/maps/api/place/details/json"
    params = {
        "placeid": placeid,
        "key": settings.GOOGLE_API_KEY,
        "fields": "formatted_phone_number",
    }
    response = requests.get(url, params)
    data = json.loads(response.content)
    if data["status"] != "OK":
        raise PhoneNotFound("Phone not found for placeid %s" % placeid)

    return data["result"].get("formatted_phone_number")


def geocode(text):
    try:
        data = find_place(text)
        location = data["geometry"]["location"]
        return Point(location["lng"], location["lat"])
    except GeoCodeException as e:
        logger.error(e)
