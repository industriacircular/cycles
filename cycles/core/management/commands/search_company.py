from django.core.management.base import BaseCommand
import logging
from cycles.core.models import Company
from cycles.core.geocode import find_place, get_phone, GeoCodeException
from django.contrib.gis.geos import Point


logger = logging.getLogger()


def save_place(data):
    company = Company()
    company.name = data.get("name")
    company.address = data.get("formatted_address")
    company.phone = data.get("formatted_phone_number")
    geometry = data.get("geometry")
    if geometry:
        lng = data["geometry"]["location"]["lng"]
        lat = data["geometry"]["location"]["lat"]
        company.location = Point(lng, lat)
    company.save()
    logger.info("Saved company: %r" % company)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            "-d",
            action="store_true",
            dest="dry_run",
            help="Don't really hit Google or save anything to the database",
        )
        parser.add_argument(
            "--file",
            "-f",
            dest="FILE",
            help="Use each line of FILE as input",
        )
        parser.add_argument(
            "NAME",
            type=str,
            help="Name of the company to search for",
            nargs="?",
        )

    def handle(self, *args, **options):
        self.options = options

        if options["FILE"]:
            with open(options["FILE"], "r") as f:
                for line in f.readlines():
                    text = line.strip()
                    if text:
                        self.handle_input(text)
        if options["NAME"]:
            self.handle_input(options["NAME"])

    def handle_input(self, text):
        if self.options["dry_run"]:
            logger.warning('Would search: "%s" (doing nothing due to --dry-run)' % text)
            return
        try:
            place = find_place(text)
            plid = place.get("place_id")
            if plid:
                place["formatted_phone_number"] = get_phone(plid)
            save_place(place)
        except GeoCodeException as e:
            logger.error(e)
