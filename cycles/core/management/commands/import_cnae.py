from django.core.management.base import BaseCommand
from django.template.defaultfilters import capfirst
import json
import logging
import requests
from cycles.core.models import FieldOfActivity


logger = logging.getLogger()


ENDPOINT = "https://servicodados.ibge.gov.br/api/v2/cnae/secoes/{sec}/subclasses"


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "SECTION",
            type=str,
            help="CNAE sections to import (A, B, C ...)",
            nargs="+",
        )

    def handle(self, *args, **options):
        sections = "|".join(options["SECTION"])
        logger.info(f"loading sections: {sections}")
        url = ENDPOINT.format(sec=sections)
        response = requests.get(url)
        data = json.loads(response.content)
        seen = {}
        for item in data:
            self.import_field(item, seen, ["classe", "grupo", "divisao"])

    def import_field(self, item, seen, hierarchy):
        field = self.save_field(item, seen)
        if field and hierarchy and hierarchy[0] in item:
            self.import_field(item[hierarchy[0]], seen, hierarchy[1:])

    def save_field(self, item, seen):
        if item["id"] in seen:
            return None
        field, _ = FieldOfActivity.objects.get_or_create(code=item["id"])
        field.name = capfirst(item["descricao"].lower())
        field.save()
        seen[field.code] = True
        logger.info(f"Saving {field}")
        return field
