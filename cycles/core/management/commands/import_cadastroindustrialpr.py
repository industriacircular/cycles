from django.core.management.base import BaseCommand
import json
import logging
from cycles.core.models import Company
from cycles.core.models import FieldOfActivity


logger = logging.getLogger()


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "FILE",
            type=open,
            help="Files to import",
            nargs="+",
        )

    def handle(self, *args, **options):
        for f in options["FILE"]:
            self.import_file(f)

    def import_file(self, f):
        logger.info(f"Loading {f.name}")
        data = json.load(f)
        for item in data["records"]:
            self.import_item(item)

    def import_item(self, item):
        company, _ = Company.objects.get_or_create(cnpj=item["cnpj"])
        company.name = item["fantasia"] or item["nome"]
        company.full_name = item["nome"]
        company.email = item["email"]
        company.phone = " ".join([item["ddd_telefone"], item["telefone"]])
        addr = [item[k] for k in ["endereco", "municipio", "uf", "cep"] if item[k]]
        company.address = " ".join(addr)
        company.field_of_activity, _ = FieldOfActivity.objects.get_or_create(
            code=item["cnae"]
        )
        company.save()

        logger.info(f'Imported {item["nome"]}')
