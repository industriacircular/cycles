# -*- coding: utf-8 -*-
# Generated by Django 1.11.21 on 2019-06-19 20:59
from __future__ import unicode_literals

import cycles.core.models
import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0007_raw_materials_and_residues"),
    ]

    operations = [
        migrations.CreateModel(
            name="TransportCompany",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=128, verbose_name="Name")),
                (
                    "full_name",
                    models.CharField(
                        blank=True,
                        help_text="E.g. the legal name of the company",
                        max_length=256,
                        null=True,
                        verbose_name="Full name",
                    ),
                ),
                (
                    "cnpj",
                    models.CharField(
                        blank=True,
                        max_length=18,
                        null=True,
                        unique=True,
                        validators=[cycles.core.models.validate_cnpj],
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        blank=True, max_length=254, null=True, verbose_name="Email"
                    ),
                ),
                (
                    "phone",
                    models.CharField(
                        blank=True, max_length=40, null=True, verbose_name="Phone"
                    ),
                ),
                (
                    "address",
                    models.TextField(blank=True, null=True, verbose_name="Address"),
                ),
                (
                    "location",
                    django.contrib.gis.db.models.fields.PointField(
                        blank=True, null=True, srid=4326, verbose_name="Location"
                    ),
                ),
            ],
            options={
                "verbose_name": "Transport company",
                "verbose_name_plural": "Transport companies",
            },
        ),
    ]
