# Generated by Django 2.2.12 on 2020-04-06 22:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0014_material_use_verbose_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="rawmaterialused",
            name="quantity_unit",
            field=models.CharField(
                blank=True,
                choices=[
                    ("kg/d", "Kilograms per day"),
                    ("kg/w", "Kilograms per week"),
                    ("kg/m", "Kilograms per month"),
                    ("kg/y", "Kilograms per year"),
                    ("t/d", "Tons per day"),
                    ("t/w", "Tons per week"),
                    ("t/m", "Tons per month"),
                    ("t/y", "Tons per year"),
                    ("l/d", "Liters per day"),
                    ("l/w", "Liters per week"),
                    ("l/m", "Liters per month"),
                    ("l/y", "Liters per year"),
                    ("un/d", "Units per day"),
                    ("un/w", "Units per week"),
                    ("un/m", "Units per month"),
                    ("un/y", "Units per year"),
                ],
                max_length=10,
                null=True,
                verbose_name="Unit",
            ),
        ),
        migrations.AlterField(
            model_name="residueproduced",
            name="quantity_unit",
            field=models.CharField(
                blank=True,
                choices=[
                    ("kg/d", "Kilograms per day"),
                    ("kg/w", "Kilograms per week"),
                    ("kg/m", "Kilograms per month"),
                    ("kg/y", "Kilograms per year"),
                    ("t/d", "Tons per day"),
                    ("t/w", "Tons per week"),
                    ("t/m", "Tons per month"),
                    ("t/y", "Tons per year"),
                    ("l/d", "Liters per day"),
                    ("l/w", "Liters per week"),
                    ("l/m", "Liters per month"),
                    ("l/y", "Liters per year"),
                    ("un/d", "Units per day"),
                    ("un/w", "Units per week"),
                    ("un/m", "Units per month"),
                    ("un/y", "Units per year"),
                ],
                max_length=10,
                null=True,
                verbose_name="Unit",
            ),
        ),
    ]
