# Generated by Django 2.2.6 on 2019-12-06 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0010_extra_time_units"),
    ]

    operations = [
        migrations.AlterField(
            model_name="material",
            name="level",
            field=models.PositiveIntegerField(editable=False),
        ),
        migrations.AlterField(
            model_name="material",
            name="lft",
            field=models.PositiveIntegerField(editable=False),
        ),
        migrations.AlterField(
            model_name="material",
            name="rght",
            field=models.PositiveIntegerField(editable=False),
        ),
    ]
