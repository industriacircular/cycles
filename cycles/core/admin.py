from django.contrib.admin import site
from django.contrib.admin import SimpleListFilter
from django.urls import re_path
from django.core.serializers import serialize
from django.contrib.gis import admin
from django.http import HttpResponse
from django.template.defaultfilters import truncatechars
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as N_
from django.utils.translation import gettext_lazy as __
from django.utils.translation import ugettext as _
from mptt.admin import MPTTModelAdmin
from cycles.compat import ModelAdminCompatMixin
from cycles.core import models
from cycles.settings import SITE_NAME


site.site_title = SITE_NAME
site.site_header = SITE_NAME


class OpportunitiesMixin(object):
    def get_urls(self):
        return [
            re_path(
                "^(.+)/opportunities/$",
                self.admin_site.admin_view(self.opportunities),
                name=self.opportunities_link,
            ),
        ] + super().get_urls()

    def get_context(self, obj):
        raise NotImplementedError()

    def opportunities(self, request, pk):
        obj = self.model.objects.get(pk=pk)
        context = self.admin_site.each_context(request).copy()
        context["opts"] = self.opts
        context["has_change_permission"] = self.has_change_permission(request, obj)
        context["original"] = _("%s: oportunitties map") % obj
        context.update(self.get_context(obj))
        return TemplateResponse(request, self.opportunities_template, context)

    def links(self, obj):
        link = reverse("admin:" + self.opportunities_link, args=(obj.pk,))
        return mark_safe('<a href="%s">%s</a>' % (link, _("Opportunities map")))


class RawMaterialAdmin(OpportunitiesMixin, admin.TabularInline):
    model = models.Company.raw_materials.through
    fk_name = "company"
    extra = 0
    verbose_name = N_("Raw material used")
    verbose_name_plural = N_("Raw materials used")


class ResidueProducedAdmin(OpportunitiesMixin, admin.TabularInline):
    model = models.Company.residues_produced.through
    fk_name = "company"
    extra = 0
    verbose_name = N_("Residue produced")
    verbose_name_plural = N_("Residues produced")


class FieldOfActivityFilter(SimpleListFilter):
    title = N_("Field of Activity")
    parameter_name = "field_of_activity"

    def lookups(self, request, model_admin):
        items = models.FieldOfActivity.objects.filter(use_as_filter=True)
        return tuple(
            [(("_"), N_("Not informed"))] + [(item.code, str(item)) for item in items]
        )

    def queryset(self, request, queryset):
        prefix = self.value()
        if prefix == "_":
            return queryset.filter(field_of_activity__isnull=True)
        elif prefix:
            return queryset.filter(field_of_activity__code__startswith=self.value())
        else:
            return queryset


class HasGeoLocationFilter(SimpleListFilter):
    title = N_("Has geolocation")
    parameter_name = "has_geolocation"

    def lookups(self, request, model_admin):
        return (
            ("0", __("No")),
            ("1", __("Yes")),
        )

    def queryset(self, request, queryset):
        if self.value() == "0":
            return queryset.filter(location__isnull=True)
        elif self.value() == "1":
            return queryset.filter(location__isnull=False)
        else:
            return queryset


def geocode(modeladmin, request, queryset):
    for item in queryset:
        item.geocode()
        item.save()


geocode.short_description = N_("Geocode selected items")


@admin.register(models.Company)
class CompanyAdmin(OpportunitiesMixin, ModelAdminCompatMixin, admin.OSMGeoAdmin):
    list_display = ("name", "field_of_activity_display", "validated", "has_geolocation")
    list_filter = ("validated", HasGeoLocationFilter, FieldOfActivityFilter)
    search_fields = ("name", "full_name", "address", "field_of_activity__name")
    readonly_fields = ("location", "self_service_materials_link")
    inlines = [RawMaterialAdmin, ResidueProducedAdmin]
    actions = [geocode]

    def has_geolocation(self, obj):
        return bool(obj.location)

    has_geolocation.boolean = True
    has_geolocation.short_description = N_("Has geolocation")

    def field_of_activity_display(self, obj):
        field = obj.field_of_activity
        return field and truncatechars(str(field), 60)

    field_of_activity_display.short_description = N_("Field of Activity")

    def self_service_materials_link(self, obj):
        link = reverse("selfservice-materials", args=[obj.secret_code])
        return mark_safe(f"<a href='{link}'>{link}</a>")

    self_service_materials_link.short_description = N_("Materials Form Link")

    # config for OpportunitiesMixin
    opportunities_link = "core_company_opportunities"
    opportunities_template = "opportunities.html"

    def get_context(self, company):
        return {
            "company": company,
        }

    def get_urls(self):
        urls = [
            re_path(
                "map/",
                self.admin_site.admin_view(self.map),
                name="core_company_map",
            ),
            re_path(
                "mapdata/",
                self.admin_site.admin_view(self.mapdata),
                name="core_company_mapdata",
            ),
        ]
        return urls + super().get_urls()

    change_list_template = "companies.html"

    def map(self, request):
        context = {
            "title": N_("Map of companies"),
            "cl": type(self),
        }
        return TemplateResponse(request, "companies_map.html", context)

    def mapdata(self, request):
        try:
            request.GET._mutable = True  # FIXME horrible hack
            south = float(request.GET.pop("south")[0])
            north = float(request.GET.pop("north")[0])
            west = float(request.GET.pop("west")[0])
            east = float(request.GET.pop("east")[0])
            request.GET._mutable = False  # FIXME horrible hack
        except ValueError:
            return HttpResponse("{}", status=400, content_type="application/json")

        changelist = self.get_changelist_instance(request)

        # apply all filters and search from the query string
        companies = changelist.get_queryset(request)

        # now filter by region
        region = models.get_polygon(
            south=south,
            north=north,
            west=west,
            east=east,
        )
        companies = companies.filter(
            location__contained=region,
        )

        data = serialize("geojson", companies)
        return HttpResponse(data, content_type="application/json")


@admin.register(models.TransportCompany)
class TransportCompanyAdmin(admin.OSMGeoAdmin):
    search_fields = ("name", "full_name", "address")
    readonly_fields = ("location",)


admin.site.register(models.Product)


class ApplicationsAdmin(admin.TabularInline):
    model = models.Material.applications.through
    verbose_name = N_("Application")
    verbose_name_plural = N_("Applications")
    extra = 0


class MaterialSubTypesAdmin(admin.TabularInline):
    model = models.Material
    fk_name = "parent"
    verbose_name = N_("Subtype")
    verbose_name_plural = N_("Subtypes")
    fields = ["name", "notes"]
    show_change_link = True
    extra = 0


class ListMaterialSubTypesAdmin(MaterialSubTypesAdmin):
    readonly_fields = ["name", "notes"]

    def has_add_permission(self, request):
        return False


class AddMaterialSubTypesAdmin(MaterialSubTypesAdmin):
    verbose_name_plural = ""

    def has_change_permission(self, request, *args):
        return False


class CompanyList(admin.TabularInline):
    show_change_link = True

    def has_add_permission(self, request, *args):
        return False

    def has_delete_permission(self, request, *args):
        return False


class ProducersList(CompanyList):
    model = models.ResidueProduced
    verbose_name = N_("Producer")
    verbose_name_plural = N_("Producers")


class ConsumersList(CompanyList):
    model = models.RawMaterialUsed
    verbose_name = N_("Consumer")
    verbose_name_plural = N_("Consumers")


@admin.register(models.Material)
class MaterialAdmin(MPTTModelAdmin):
    list_display = ("name",)
    list_filter = ("level",)
    list_per_page = 500
    search_fields = ("name",)
    exclude = ["applications"]
    inlines = [
        ListMaterialSubTypesAdmin,
        AddMaterialSubTypesAdmin,
        ApplicationsAdmin,
        ProducersList,
        ConsumersList,
    ]

    def applications_count(self, obj):
        return obj.applications_count

    applications_count.short_description = N_("Applications")

    def producers_count(self, obj):
        return obj.producers_count

    producers_count.short_description = N_("Producers")

    def consumers_count(self, obj):
        return obj.consumers_count

    consumers_count.short_description = N_("Consumers")


@admin.register(models.FieldOfActivity)
class FieldOfActivityAdmin(admin.ModelAdmin):
    pass


@admin.register(models.PackagingType)
class PackagingTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(models.DestinationType)
class DestinationTypeAdmin(admin.ModelAdmin):
    pass
