from django.conf import settings


def expose_settings(request):
    return {
        "SITE_NAME": settings.SITE_NAME,
        "CURRENCY": settings.CURRENCY,
    }
