from cycles.settings import *  # noqa

MIDDLEWARE.remove("whitenoise.middleware.WhiteNoiseMiddleware")  # noqa
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"
