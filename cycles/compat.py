from django.contrib.admin import ModelAdmin


try:
    # Django 2+
    ModelAdmin.get_changelist_instance

    class ModelAdminCompatMixin:
        pass


except AttributeError:
    # Django 1
    class ModelAdminCompatMixin:
        def get_changelist_instance(self, request):
            list_display = self.get_list_display(request)
            list_display_links = self.get_list_display_links(request, list_display)
            list_filter = self.get_list_filter(request)
            search_fields = self.get_search_fields(request)
            list_select_related = self.get_list_select_related(request)
            ChangeList = self.get_changelist(request)
            return ChangeList(
                request,
                self.model,
                list_display,
                list_display_links,
                list_filter,
                self.date_hierarchy,
                search_fields,
                list_select_related,
                self.list_per_page,
                self.list_max_show_all,
                self.list_editable,
                self,
            )
